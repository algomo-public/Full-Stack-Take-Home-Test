
# Mock Messaging API Specification

## Endpoints

### Conversations

Returns a list of conversations. Results are paginated (25 conversations per page). Note that the conversations don't include last message.

- Path: `/conversations`
- Method: GET
- Params: page=[page]
- Example request: `/conversations?page=1`
- Success response:

```
{
    "data": [
        { id: 1, language: "en" },
        ...
    ],
    "page": 1,
    "totalPages": 4
}
```

- Error response:

```
{
    "code": 404,
    "message": "Page not found"
}
```

### Messages

Returns all messages for a conversation.

- Path: `/messages/[conversation_id]`
- Method: GET
- Example request: `/messages/3`
- Success response:

```
[
    {
        "id": 631,
        "authorType": "bot",
        "text": "Pourquoi faites-vous vérifier votre identité?",
        "conversationId": 92,
        "time": "2022-07-22T09:27:11.639Z"
    }
]
```

### Languages

Returns all supported languages.

- Path: `/languages`
- Method: GET
- Example request: `/languages`
- Success response:

```
[
    "en",
    "es",
    "el",
    ...
]
```

## Notes

- Conversations and languages are static. They will not be updated during runtime.
- Messages are dynamic. On each request to the messages endpoint, a new message might be created. Ensure that the data on the frontend is always up to date.
- Each conversation will have at least one message.
