# Full Stack Take Home Test

**Hi** 👋 

Thank you for taking the time to do this technical test.

**Read the instructions carefully.**
Your ability to understand and follow instructions is part of the evaluation.

The challenge has two parts:

1) Completeting a technical [task](#task) where you will be evaluated according to some [criteria](#evaluation-points-in-order-of-importance)


2) Preparing some [follow-up questions](docs/follow-up-questions.md)  (to be discussed in the in-person interview after your submission)


# Instructions and submission guidelines


- [ ] Clone this repository (do **not** fork it)
- [ ] Build the application
- [ ] Publish it on your private GitHub (or Gitlab, or whatever...). **DO NOT make it public.**
- [ ] Invite tech@algomo.com to your repo and tell us approximatively how much time you spent on this assignment

If you have any issues feel free to drop us an email at tech@algomo.com or even better, ask the bot on [our site](https://algomo.com) to talk to a human during standard working hours.

Feel free to change anything in the repo including this file to provide your documentation.

----

# Task
Create a toy full-stack application for browsing conversations and messages

This application should consist of a frontend and a backend.

## Frontend

A single-page React application consisting of three components:

1) Paginated list of conversations provided by the backend.
Each conversation can show multiple fields, for example: 
   - last sender
   - time
   - language
   - last message
2) Dropdown component for filtering conversations in the list by language.
3) Conversation preview. It should display all messages from a conversation selected in the conversations list.

Feel free to use the layout below as a starting point, or create your own original design.

![Starter layout](layout.png)

### Details

- Use pagination to display **at most 10 conversations per page**.
- **Sort messages by date**. Conversations can be displayed as they are returned from the backend.
- **Both filtering and pagination should happen on the backend.** Load only the essential data.
- Feel free to use a component library or CSS framework of your choice, or go with pure CSS if you want to showcase your styling skills.
- Use a bootstrapping tool, preferably [Vite with TypeScript template](https://vitejs.dev/guide/#scaffolding-your-first-vite-project) and any other libraries you would normally use.

We don't specify all the details on purpose. We're happy as long as the UX is great!

## Backend

A NodeJS application serving data to the frontend with 3 endpoints:

1) **Conversations endpoint:** return a list of conversations. Results should be paginated (10 per page) and each conversation should include a single last message.
2) **Messages endpoint:** return a list of all messages from a specific conversation.
3) **Languages endpoint:** return a list of all supported languages.

### Data

Use the Mock Messaging API to get conversation data for use in this tech task. We distribute it in a form of a Docker image. It is already set-up for you in the `docker-compose.yml` file.

Please refer to the [docs/mock-messaging-api.md](docs/mock-messaging-api.md) file for the API specification.

Note that **only the backend service should be able to communicate with the Messaging API**. 


### Details

- Ensure that only the essential data is returned, i.e. don't return more than 10 conversations per request.
- To ensure smooth operation of the backend, **create three replicas of this service**. You can expect us to randomly disable one or two of them during our evaluation.

----

# Evaluation points in order of importance

- understanding the challenge and requirements
- use of clean code, which is self documenting
- use of packages to achieve separation of concerns
- use of domain driven design
- dockerize the application. You should only have to run `docker compose build` and `docker compose up` to run it.
- tests for business logic
- user experience for frontend
- performance 
- use of code quality checks such as linters and build tools
- use of git with appropriate commit messages
- documentation: README and inline code comments
- code should be written as if it was going to be deployed today

Your code needs to be easy to follow and to maintain for another developer. We advise you to focus your time in writing the your best code, not in writing a lot of code. 

This assignment is meant to evaluate senior full-stack engineers.

Our evaluation will focus primarily on your ability to follow good design principles and less on correctness and completeness of algorithms. During the face to face interview (see [follow-up questions](docs/follow-up-questions.md) ) you will have the opportunity to explain your choices and provide justifications for the parts that you omitted.


Time limitations: **there are no hard time limits**, but please don't spend more than ~3 hours.
If you do, please let us know. 
You will **not** be judged on how much time you spent on it, but we need to know so that we can improve the test)


# Summary

As you can see, this test leaves you a fair amount of flexibility. This emulates the way we work. We are a dynamic start-up and a successful candidate will often have to deal with similar uncertainty.

Although we throw a few curveballs at you, we hope this test is enjoyable. We'd love to hear your feedback.

Good luck, thank you for your time, and we look forward to hearing from you!
